print("""
 
 
  .-"-.         .-"-.
 /     \ _____ /     \
 \      `     `      /
  `--/,'^^'.'^^',\--` 
    || /^\   /^\ |\
    |\ |0| _ |0| /|
    \_\`~`.-.`~`/_/
    /     '-'     \
    \  `-,...,-`  /
  '-._ \^/ _.-'

  
  APP CÁLCULO DO INDICE
  DE MASSA CORPORAL
""")

nome=input("Digite seu nome?")
altura=float( input("Qual é a sua altura (em metros)?"))
peso=float(input("Qual seu peso(em kg)?"))

imc= peso/altura**2

peso0=18.5 *(altura**2)
peso1=24.9*(altura**2)
print(f"O IMC de {nome} é: {imc:.4f} ")

if(imc<18.5) or (imc>25):
    print(f"O seu peso deveria estar entre:{peso0 :.1f} e {peso1:.1f}")
else:
    print("Parabéns!! Você está com o IMC normal")   
